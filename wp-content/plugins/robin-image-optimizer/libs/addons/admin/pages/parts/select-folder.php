<h2><?php _e( 'Select folder', 'robin-image-optimizer' ); ?></h2>
<div class="rio-folder-picker" style="max-height:400px;overflow: auto;border: 1px solid #888;"></div>
<input id="wbcr-rio-selected-path" disabled type="text" style="width:100%;">
<button id="wbcr-rio-select-btn"><?php _e( 'Select', 'robin-image-optimizer' ); ?></button>
<p id="wbcr-rio-indexing-text" style="display:none;"><?php _e( 'The selected directory is being indexed. Found', 'robin-image-optimizer' ); ?> <span class="wbcr-rio-indexing-counter">0</span> <?php _e( 'images.', 'robin-image-optimizer' ); ?></p>
<p id="wbcr-rio-indexing-finish-text" style="display:none;"><?php _e( 'Indexing complete. Directory successfully added and ready for optimization.', 'robin-image-optimizer' ); ?></p>
