<?php
$a1 = $a2 ="";
if( have_rows('gallery_images') ):
    // loop through the rows of data
	while ( have_rows('gallery_images') ) : the_row();
		$room_image = get_sub_field('gallery_uploadimage');
		if(trim($room_image) !=="" ){
			if(strpos($room_image , 's7.shawimg.com') !== false){
				if(strpos($room_image , 'http') === false){ 
					$room_image = "http://" . $room_image;
				}	
				$a1 = $a2 = $room_image ;
			}else{
				if(strpos($room_image , 'http') === false){ 
					$room_image = "https://" . $room_image;
				}	
				$a1 = "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[300x300]&sink";
				$a2 = "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[600x400]&sink";

			}	
	  		break;
		}
	endwhile;
endif;

if($a1 !=""){	
?>
<div <?php post_class('fl-post-grid-post open-gallery-modal'); ?> itemscope itemtype="<?php FLPostGridModule::schema_itemtype(); ?>">
	
	<?php FLPostGridModule::schema_meta(); ?>

	<?php if(has_post_thumbnail() && $settings->show_image) : ?>
	<div class="fl-post-grid-image">
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
			<?php the_post_thumbnail($settings->image_size); ?>
		</a>
	</div>
	<?php endif; ?>
	
	
	<div class="fl-post-grid-gallery" style="background: url('<?php echo $a1; ?>') no-repeat;background-size: cover;">
	</div>
	


	<div class="gallery-modal">
		<div class="fl-row-content-wrap">
			<div class="content">
				<div class="col col-sm-6 col-md-8 col-lg-8 col_swatch" style="background-image:url('<?php echo $a2; ?>')">
					&nbsp;
				</div>
				<div class="col col-sm-6 col-md-4 col-lg-4 product-box">
					<a href="#" class="close_modal"><i class="fa fa-times"></i></a>
	                <h2 class="collection"><?php the_field('collection'); ?></h2>
	                <h1 class="fl-post-title" itemprop="name">
	                   <?php the_field('color'); ?>
	                </h1>

	                <div class="product-colors">
	                    <?php
						global $post;
						 
	                    $familysku = get_post_meta($post->ID, 'collection', true);						
	                    if(is_singular( 'laminate_catalog' )){
                        $flooringtype = 'laminate_catalog';
                    } elseif(is_singular( 'hardwood_catalog' )){
                        $flooringtype = 'hardwood_catalog';
                    } elseif(is_singular( 'carpeting' )){
                        $flooringtype = 'carpeting';
                    } elseif(is_singular( 'luxury_vinyl_tile' )){
                        $flooringtype = 'luxury_vinyl_tile';
                    } elseif(is_singular( 'vinyl' )){
                        $flooringtype = 'vinyl';
                    } elseif(is_singular( 'solid_wpc_waterproof' )){
                        $flooringtype = 'solid_wpc_waterproof';
                    } elseif(is_singular( 'tile_catalog' )){
                        $flooringtype = 'tile_catalog';
                    } ?>


                        <?php if (is_page( 19677 )) { ?>
                            <?php
                            $args = array(
                                'post_type'      => array( 'carpeting', 'hardwood', 'laminate', 'luxury_vinyl_tile', 'vinyl', 'tile', 'solid_wpc_waterproof'  ),
                                'posts_per_page' => -1,
                                'post_status'    => 'publish',
                                'meta_query' => array(
                                    array(
                                        'key' => 'in_stock',
                                        'compare' => '==',
                                        'value' => '1'
                                    ))
                            );
                            ?>

                       <?php  } else { ?>
                            <?php
                            $args = array(
                                'post_type'      => $flooringtype,
                                'posts_per_page' => -1,
                                'post_status'    => 'publish',
                                'meta_query'     => array(
                                    array(
                                        'key'     => 'collection',
                                        'value'   => $familysku,
                                        'compare' => '='
                                    )
                                )
                            );
                            ?>

                       <?php } ?>







	                    <?php
	                 
						
						 $the_query = new WP_Query( $args );
					
	                    ?>
	                    <?php  //echo $the_query ->found_posts; ?> <!-- Colors Available -->
	                </div>

					<br>
	                <a href="<?php the_permalink() ?>" class="view_more fl-button"><span class="fl-button-text"><?php _e("View More","fl-builder"); ?></span></a>
					<br>
	                <a href="<?php echo get_permalink(13) ?>" class="contact_us fl-button"><span class="fl-button-text"><?php _e("Contact Us","fl-builder"); ?></span></a>
				</div>
			</div>
		</div>
	</div>

</div>
<?php } ?>